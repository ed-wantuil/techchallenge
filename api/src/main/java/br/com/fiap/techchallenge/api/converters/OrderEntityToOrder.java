package br.com.fiap.techchallenge.api.converters;

import java.util.Objects;

import br.com.fiap.techchallenge.api.entities.OrderEntity;
import br.com.fiap.techchallenge.customer.domains.Order;
import br.com.fiap.techchallenge.customer.vos.OrderAmount;
import br.com.fiap.techchallenge.customer.vos.OrderStatus;

public class OrderEntityToOrder {

    private OrderEntityToOrder() {
        throw new IllegalStateException("Utility class");
    }

    public static Order convert(OrderEntity orderEntity) {
        return Order
                .builder()
                .id(orderEntity.getId().toString())
                .customer(Objects.nonNull(orderEntity.getCustomer()) ? CustomerEntityToCustomer.convert(orderEntity.getCustomer()) : null)
                .items(orderEntity.getItems().stream().map(OrderItemEntityToOrderItem::convert).toList())
                .status(new OrderStatus(orderEntity.getStatus()))
                .created(orderEntity.getCreated())
                .amount(new OrderAmount(orderEntity.getAmount()))
                .build();
    }
}
