package br.com.fiap.techchallenge.api.controllers;

import br.com.fiap.techchallenge.customer.dtos.CustomerDTO;
import br.com.fiap.techchallenge.customer.ports.CustomerServicePort;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class CustomerController {
    private final CustomerServicePort customerService;
    private final Logger log = LoggerFactory.getLogger(CustomerController.class);

    @Operation(summary = "Create a new customer",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Customer created")
            })
    @PostMapping("/customer")
    CustomerDTO create(@RequestBody CustomerDTO customerDTO) {
        log.info("Creating customer {}", customerDTO);

        return customerService.create(customerDTO);
    }

    @Operation(summary = "Find by CPF",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Customer found"),
                    @ApiResponse(responseCode = "404", description = "Customer not found")
            })
    @GetMapping("/customer")
    CustomerDTO findByCpf(@RequestParam String cpf) {
        log.info("Finding customer by CPF {}", cpf);

        return customerService.findByCpf(cpf);
    }
}
