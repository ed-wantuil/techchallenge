package br.com.fiap.techchallenge.api.controllers;

import java.util.List;

import br.com.fiap.techchallenge.customer.dtos.OrderDTO;
import br.com.fiap.techchallenge.customer.ports.OrderServicePort;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class OrderController {
    private final OrderServicePort orderService;
    private final Logger log = LoggerFactory.getLogger(OrderController.class);

    @Operation(summary = "Checkout a new order",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Checkout a new order")
            })
    @PostMapping("/checkout")
    OrderDTO create(@RequestBody OrderDTO orderDTO) {
        log.info("Creating product {}", orderDTO);

        return orderService.checkout(orderDTO);
    }

    @Operation(summary = "Find orders by status",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Find orders by status"),
            })
    @GetMapping("/order")
    List<OrderDTO> get(@RequestParam String status) {
        return orderService.findByStatus(status);
    }
}
