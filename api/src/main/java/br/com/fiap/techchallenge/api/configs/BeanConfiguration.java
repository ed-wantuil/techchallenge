package br.com.fiap.techchallenge.api.configs;

import br.com.fiap.techchallenge.api.Application;
import br.com.fiap.techchallenge.attendant.adapters.ProductService;
import br.com.fiap.techchallenge.attendant.ports.ProductRepositoryPort;
import br.com.fiap.techchallenge.attendant.ports.ProductServicePort;
import br.com.fiap.techchallenge.customer.adapters.CustomerService;
import br.com.fiap.techchallenge.customer.adapters.OrderService;
import br.com.fiap.techchallenge.customer.ports.CustomerRepositoryPort;
import br.com.fiap.techchallenge.customer.ports.CustomerServicePort;
import br.com.fiap.techchallenge.customer.ports.OrderRepositoryPort;
import br.com.fiap.techchallenge.customer.ports.OrderServicePort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = Application.class)
public class BeanConfiguration {

    @Bean
    ProductServicePort productService(ProductRepositoryPort productRepositoryPort) {
        return new ProductService(productRepositoryPort);
    }

    @Bean
    CustomerServicePort customerService(CustomerRepositoryPort customerRepositoryPort) {
        return new CustomerService(customerRepositoryPort);
    }

    @Bean
    OrderServicePort orderService(OrderRepositoryPort orderRepositoryPort) {
        return new OrderService(orderRepositoryPort);
    }
}
