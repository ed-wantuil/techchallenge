package br.com.fiap.techchallenge.api.converters;

import br.com.fiap.techchallenge.api.entities.OrderItemEntity;
import br.com.fiap.techchallenge.customer.vos.OrderItem;

public class OrderItemEntityToOrderItem {

    private OrderItemEntityToOrderItem() {
        throw new IllegalStateException("Utility class");
    }

    public static OrderItem convert(OrderItemEntity orderItemEntity) {
        return OrderItem
                .builder()
                .product(orderItemEntity.getProduct().toString())
                .quantity(orderItemEntity.getQuantity())
                .price(orderItemEntity.getPrice())
                .build();
    }
}
