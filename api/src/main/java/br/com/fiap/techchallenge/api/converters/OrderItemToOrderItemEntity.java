package br.com.fiap.techchallenge.api.converters;

import java.util.Objects;
import java.util.UUID;

import br.com.fiap.techchallenge.api.entities.OrderItemEntity;
import br.com.fiap.techchallenge.customer.vos.OrderItem;

public class OrderItemToOrderItemEntity {

    private OrderItemToOrderItemEntity() {
        throw new IllegalStateException("Utility class");
    }

    public static OrderItemEntity convert(OrderItem orderItem) {
        return OrderItemEntity
                .builder()
                .product(Objects.nonNull(orderItem.getProduct()) ? UUID.fromString(orderItem.getProduct()) : null)
                .quantity(orderItem.getQuantity())
                .price(orderItem.getPrice())
                .build();
    }
}
