package br.com.fiap.techchallenge.api.converters;

import java.util.Objects;
import java.util.UUID;

import br.com.fiap.techchallenge.api.entities.CustomerEntity;
import br.com.fiap.techchallenge.customer.domains.Customer;

public class CustomerToCustomerEntity {

    private CustomerToCustomerEntity() {
        throw new IllegalStateException("Utility class");
    }

    public static CustomerEntity convert(Customer customer) {
        return CustomerEntity
                .builder()
                .id(Objects.nonNull(customer.getId()) ? UUID.fromString(customer.getId()) : null)
                .name(customer.getName().getName())
                .email(customer.getEmail().getEmail())
                .cpf(customer.getCpf().getCpf())
                .build();
    }
}
