package br.com.fiap.techchallenge.api.converters;

import java.util.Objects;
import java.util.UUID;

import br.com.fiap.techchallenge.api.entities.ProductEntity;
import br.com.fiap.techchallenge.attendant.domains.Product;

public class ProductToProductEntity {
    private ProductToProductEntity() {
        throw new IllegalStateException("Utility class");
    }

    public static ProductEntity convert(Product product) {
        return ProductEntity
                .builder()
                .id(Objects.nonNull(product.getId()) ? UUID.fromString(product.getId()) : null)
                .name(product.getName().getName())
                .category(product.getCategory().getCategory().name())
                .price(product.getPrice().getPrice())
                .description(product.getDescription().getDescription())
                .image(product.getImage().getImage())
                .build();
    }
}
