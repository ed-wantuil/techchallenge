package br.com.fiap.techchallenge.api.controllers;

import java.util.List;

import br.com.fiap.techchallenge.attendant.dtos.ProductDTO;
import br.com.fiap.techchallenge.attendant.ports.ProductServicePort;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class ProductController {
    private final ProductServicePort productService;
    private final Logger log = LoggerFactory.getLogger(ProductController.class);

    @Operation(summary = "Create a new product",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Product created")
            })
    @PostMapping("/product")
    ProductDTO create(@RequestBody ProductDTO productDTO) {
        log.info("Creating product {}", productDTO);

        return productService.create(productDTO);
    }

    @Operation(summary = "Update a new product",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Update created"),
                    @ApiResponse(responseCode = "404", description = "Product not found")
            })
    @PutMapping("product/{id}")
    ProductDTO update(@PathVariable String id,
                      @RequestBody ProductDTO productDTO) {

        log.info("Updating product {}", productDTO);

        return productService.update(id, productDTO);
    }

    @Operation(summary = "List products",
            responses = {
                    @ApiResponse(responseCode = "200", description = "List products"),
                    @ApiResponse(responseCode = "404", description = "Product not found")
            })
    @GetMapping("/product")
    List<ProductDTO> get() {
        return productService.find();
    }

    @Operation(summary = "Find by category",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Find by category"),
                    @ApiResponse(responseCode = "404", description = "Product not found")
            })
    @GetMapping("/product/findByCategory")
    List<ProductDTO> get(@RequestParam String category) {
        return productService.findByCategory(category);
    }

    @Operation(summary = "Remove a product by id",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Product removed"),
                    @ApiResponse(responseCode = "404", description = "Product not found")
            })
    @DeleteMapping("product/{id}")
    void remove(@PathVariable("id") String id) {
        productService.remove(id);
    }
}
