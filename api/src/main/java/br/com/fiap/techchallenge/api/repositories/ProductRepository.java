package br.com.fiap.techchallenge.api.repositories;

import java.util.List;
import java.util.UUID;

import br.com.fiap.techchallenge.api.converters.ProductEntityToProduct;
import br.com.fiap.techchallenge.api.converters.ProductToProductEntity;
import br.com.fiap.techchallenge.attendant.domains.Product;
import br.com.fiap.techchallenge.attendant.ports.ProductRepositoryPort;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class ProductRepository implements ProductRepositoryPort {

    private final SpringDataProductRepository springDataProductRepository;

    @Override
    public List<Product> find() {
        return springDataProductRepository
                .findAll()
                .stream()
                .map(ProductEntityToProduct::convert)
                .toList();
    }

    @Override
    public List<Product> findByCategory(String category) {
        return springDataProductRepository.findByCategory(category)
                .stream()
                .map(ProductEntityToProduct::convert)
                .toList();
    }

    @Override
    public Product create(Product product) {
        var productEntity = ProductToProductEntity.convert(product);
        productEntity = springDataProductRepository.save(productEntity);

        return ProductEntityToProduct.convert(productEntity);
    }

    @Override
    public Product update(String id, Product product) {
        var productEntity = springDataProductRepository
                .findById(UUID.fromString(product.getId())).orElseThrow();

        productEntity.setName(product.getName().getName());
        productEntity.setCategory(product.getCategory().getCategory().name());
        productEntity.setPrice(product.getPrice().getPrice());
        productEntity.setDescription(product.getDescription().getDescription());
        productEntity.setImage(product.getImage().getImage());
        productEntity = springDataProductRepository.save(productEntity);

        return ProductEntityToProduct.convert(productEntity);
    }

    @Override
    public void remove(String id) {
        springDataProductRepository.deleteById(UUID.fromString(id));
    }
}
