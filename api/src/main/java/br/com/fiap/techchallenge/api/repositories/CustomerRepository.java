package br.com.fiap.techchallenge.api.repositories;

import br.com.fiap.techchallenge.api.converters.CustomerEntityToCustomer;
import br.com.fiap.techchallenge.api.converters.CustomerToCustomerEntity;
import br.com.fiap.techchallenge.customer.domains.Customer;
import br.com.fiap.techchallenge.customer.ports.CustomerRepositoryPort;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class CustomerRepository implements CustomerRepositoryPort {

    private final SpringDataCustomerRepository springDataCustomerRepository;

    @Override
    public Customer create(Customer customer) {
        var customerEntity = CustomerToCustomerEntity.convert(customer);
        customerEntity = springDataCustomerRepository.save(customerEntity);

        return CustomerEntityToCustomer.convert(customerEntity);
    }

    @Override
    public Customer findByCpf(String cpf) {
        var customerEntity = springDataCustomerRepository.findByCpf(cpf);

        return CustomerEntityToCustomer.convert(customerEntity);
    }
}
