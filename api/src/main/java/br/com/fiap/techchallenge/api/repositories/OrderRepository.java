package br.com.fiap.techchallenge.api.repositories;

import java.util.List;

import br.com.fiap.techchallenge.api.converters.OrderEntityToOrder;
import br.com.fiap.techchallenge.api.converters.OrderItemToOrderItemEntity;
import br.com.fiap.techchallenge.api.converters.OrderToOrderEntity;
import br.com.fiap.techchallenge.customer.domains.Order;
import br.com.fiap.techchallenge.customer.ports.OrderRepositoryPort;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class OrderRepository implements OrderRepositoryPort {

    private final SpringDataOrderRepository springDataOrderRepository;
    private final SpringDataOrderItemRepository springDataOrderItemRepository;

    @Override
    public Order checkout(Order order) {
        var orderEntity = OrderToOrderEntity.convert(order);
        orderEntity = springDataOrderRepository.save(orderEntity);

        for (var item : order.getItems()) {
            var orderItemEntity = OrderItemToOrderItemEntity.convert(item);
            orderItemEntity.setOrder(orderEntity);
            springDataOrderItemRepository.save(orderItemEntity);
        }

        return OrderEntityToOrder.convert(orderEntity);
    }

    @Override
    public List<Order> findByStatus(String status) {
        var orderEntities = springDataOrderRepository.findByStatus(status);

        return orderEntities
                .stream()
                .map(OrderEntityToOrder::convert)
                .toList();
    }
}
