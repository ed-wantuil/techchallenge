package br.com.fiap.techchallenge.api.converters;

import java.util.Objects;
import java.util.UUID;

import br.com.fiap.techchallenge.api.entities.OrderEntity;
import br.com.fiap.techchallenge.customer.domains.Order;

public class OrderToOrderEntity {

    private OrderToOrderEntity() {
        throw new IllegalStateException("Utility class");
    }

    public static OrderEntity convert(Order order) {
        return OrderEntity
                .builder()
                .id(Objects.nonNull(order.getId()) ? UUID.fromString(order.getId()) : null)
                .customer(Objects.nonNull(order.getCustomer()) ? CustomerToCustomerEntity.convert(order.getCustomer()) : null)
                .items(order.getItems()
                        .stream()
                        .map(OrderItemToOrderItemEntity::convert)
                        .toList())
                .amount(order.getAmount().getAmount())
                .status(order.getStatus().getStatus().toString())
                .created(order.getCreated())
                .build();
    }
}
