package br.com.fiap.techchallenge.api.converters;

import br.com.fiap.techchallenge.api.entities.ProductEntity;
import br.com.fiap.techchallenge.attendant.domains.Product;
import br.com.fiap.techchallenge.attendant.vos.ProductCategory;
import br.com.fiap.techchallenge.attendant.vos.ProductDescription;
import br.com.fiap.techchallenge.attendant.vos.ProductImage;
import br.com.fiap.techchallenge.attendant.vos.ProductName;
import br.com.fiap.techchallenge.attendant.vos.ProductPrice;

public class ProductEntityToProduct {
    private ProductEntityToProduct() {
        throw new IllegalStateException("Utility class");
    }

    public static Product convert(ProductEntity productEntity) {
        return Product
                .builder()
                .id(productEntity.getId().toString())
                .name(new ProductName(productEntity.getName()))
                .category(new ProductCategory(productEntity.getCategory()))
                .price(new ProductPrice(productEntity.getPrice()))
                .description(new ProductDescription(productEntity.getDescription()))
                .image(new ProductImage(productEntity.getImage()))
                .build();
    }
}
