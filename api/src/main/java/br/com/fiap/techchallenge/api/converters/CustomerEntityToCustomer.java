package br.com.fiap.techchallenge.api.converters;

import br.com.fiap.techchallenge.api.entities.CustomerEntity;
import br.com.fiap.techchallenge.customer.domains.Customer;
import br.com.fiap.techchallenge.customer.vos.Cpf;
import br.com.fiap.techchallenge.customer.vos.CustomerName;
import br.com.fiap.techchallenge.customer.vos.Email;

public class CustomerEntityToCustomer {

    private CustomerEntityToCustomer() {
        throw new IllegalStateException("Utility class");
    }

    public static Customer convert(CustomerEntity customerEntity) {
        return Customer
                .builder()
                .id(customerEntity.getId().toString())
                .name(new CustomerName(customerEntity.getName()))
                .email(new Email(customerEntity.getEmail()))
                .cpf(new Cpf(customerEntity.getCpf()))
                .build();
    }
}
