FROM openjdk:17
COPY ./api/build/libs/api.jar /app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]
