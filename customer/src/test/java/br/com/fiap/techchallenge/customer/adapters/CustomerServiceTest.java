package br.com.fiap.techchallenge.customer.adapters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import br.com.fiap.techchallenge.customer.domains.Customer;
import br.com.fiap.techchallenge.customer.dtos.CustomerDTO;
import br.com.fiap.techchallenge.customer.ports.CustomerRepositoryPort;
import br.com.fiap.techchallenge.customer.vos.Cpf;
import br.com.fiap.techchallenge.customer.vos.CustomerName;
import br.com.fiap.techchallenge.customer.vos.Email;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CustomerServiceTest {
    @Mock
    CustomerRepositoryPort customerRepositoryPort;

    @InjectMocks
    CustomerService customerService;

    @Test
    void shouldCreateCustomer() {
        var customer = Customer
                .builder()
                .id("032002af-0432-4aba-af57-697d46b1ffbc")
                .name(new CustomerName("Customer Name"))
                .cpf(new Cpf("123.456.789-01"))
                .email(new Email("email@mail.com"))
                .build();

        var customerDto = CustomerDTO
                .builder()
                .name("Customer Name")
                .email("email@mail.com")
                .cpf("123.456.789-01")
                .build();

        var expected = CustomerDTO
                .builder()
                .id("032002af-0432-4aba-af57-697d46b1ffbc")
                .name("Customer Name")
                .email("email@mail.com")
                .cpf("123.456.789-01")
                .build();

        Mockito.when(customerRepositoryPort.create(any())).thenReturn(customer);

        var actual = customerService.create(customerDto);

        assertEquals(expected, actual);
    }

    @Test
    void shouldFindByCpf() {
        var cpf = "123.456.789-01";

        var customers = Customer
                .builder()
                .id("032002af-0432-4aba-af57-697d46b1ffbc")
                .name(new CustomerName("Customer Name"))
                .cpf(new Cpf(cpf))
                .email(new Email("email@mail.com"))
                .build();

        var expected = CustomerDTO
                .builder()
                .id("032002af-0432-4aba-af57-697d46b1ffbc")
                .name("Customer Name")
                .email("email@mail.com")
                .cpf(cpf)
                .build();

        Mockito.when(customerRepositoryPort.findByCpf(cpf)).thenReturn(customers);

        var actual = customerService.findByCpf(cpf);

        assertEquals(expected, actual);
    }
}
