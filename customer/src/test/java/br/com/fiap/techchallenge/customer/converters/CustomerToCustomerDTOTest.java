package br.com.fiap.techchallenge.customer.converters;

import static org.junit.jupiter.api.Assertions.assertEquals;

import br.com.fiap.techchallenge.customer.domains.Customer;
import br.com.fiap.techchallenge.customer.dtos.CustomerDTO;
import br.com.fiap.techchallenge.customer.vos.Cpf;
import br.com.fiap.techchallenge.customer.vos.CustomerName;
import br.com.fiap.techchallenge.customer.vos.Email;
import org.junit.jupiter.api.Test;

class CustomerToCustomerDTOTest {
    @Test
    void shouldConverterCustomerToCustomerDTO() {
        var customer = Customer
                .builder()
                .name(new CustomerName("Customer Name"))
                .cpf(new Cpf("123.456.789-01"))
                .email(new Email("email@mail.com"))
                .build();

        var expected = CustomerDTO
                .builder()
                .name("Customer Name")
                .cpf("123.456.789-01")
                .email("email@mail.com")
                .build();

        var actual = CustomerToCustomerDTO.convert(customer);

        assertEquals(expected, actual);
    }
}
