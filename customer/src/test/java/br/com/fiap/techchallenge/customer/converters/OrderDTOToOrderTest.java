package br.com.fiap.techchallenge.customer.converters;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.List;

import br.com.fiap.techchallenge.customer.domains.Customer;
import br.com.fiap.techchallenge.customer.domains.Order;
import br.com.fiap.techchallenge.customer.dtos.CustomerDTO;
import br.com.fiap.techchallenge.customer.dtos.OrderDTO;
import br.com.fiap.techchallenge.customer.vos.Cpf;
import br.com.fiap.techchallenge.customer.vos.CustomerName;
import br.com.fiap.techchallenge.customer.vos.Email;
import br.com.fiap.techchallenge.customer.vos.OrderAmount;
import br.com.fiap.techchallenge.customer.vos.OrderItem;
import br.com.fiap.techchallenge.customer.vos.OrderStatus;
import org.junit.jupiter.api.Test;

class OrderDTOToOrderTest {
    @Test
    void shouldConvertOrderDTOToOrder() {
        var orderDTO = OrderDTO
                .builder()
                .id("54ef7c05-2da4-48fa-bba6-702129325484")
                .customer(CustomerDTO
                        .builder()
                        .id("af9eabb7-d254-473d-9c4f-ebf0b19ca5d6")
                        .cpf("123.456.789-01")
                        .email("email@mail.com")
                        .name("Customer Name")
                        .build())
                .items(List.of(OrderDTO.OrderItemDTO
                        .builder()
                        .product("5b0604c2-7e26-4e1b-affd-5eb3bdcf5297")
                        .quantity(1)
                        .price(20.)
                        .build()))
                .status("PREPARING")
                .created(LocalDate.now())
                .amount(20.)
                .build();

        var expected = Order
                .builder()
                .id("54ef7c05-2da4-48fa-bba6-702129325484")
                .customer(Customer
                        .builder()
                        .id("af9eabb7-d254-473d-9c4f-ebf0b19ca5d6")
                        .cpf(new Cpf("123.456.789-01"))
                        .email(new Email("email@mail.com"))
                        .name(new CustomerName("Customer Name"))
                        .build())
                .items(List.of(OrderItem
                        .builder()
                        .product("5b0604c2-7e26-4e1b-affd-5eb3bdcf5297")
                        .quantity(1)
                        .price(20.)
                        .build()))
                .status(new OrderStatus("PREPARING"))
                .created(LocalDate.now())
                .amount(new OrderAmount(20.))
                .build();

        var actual = OrderDTOToOrder.convert(orderDTO);

        assertEquals(expected, actual);
    }
}
