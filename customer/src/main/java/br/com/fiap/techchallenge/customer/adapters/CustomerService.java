package br.com.fiap.techchallenge.customer.adapters;

import br.com.fiap.techchallenge.customer.converters.CustomerDTOToCustomer;
import br.com.fiap.techchallenge.customer.converters.CustomerToCustomerDTO;
import br.com.fiap.techchallenge.customer.dtos.CustomerDTO;
import br.com.fiap.techchallenge.customer.ports.CustomerRepositoryPort;
import br.com.fiap.techchallenge.customer.ports.CustomerServicePort;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class CustomerService implements CustomerServicePort {
    private final CustomerRepositoryPort customerRepositoryPort;

    @Override
    public CustomerDTO create(CustomerDTO customerDTO) {
        var customer = CustomerDTOToCustomer.convert(customerDTO);
        customer = customerRepositoryPort.create(customer);

        return CustomerToCustomerDTO.convert(customer);
    }

    @Override
    public CustomerDTO findByCpf(String cpf) {
        var customer = customerRepositoryPort.findByCpf(cpf);

        return CustomerToCustomerDTO.convert(customer);
    }
}
