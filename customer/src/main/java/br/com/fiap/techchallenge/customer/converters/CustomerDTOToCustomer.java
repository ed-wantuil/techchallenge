package br.com.fiap.techchallenge.customer.converters;

import br.com.fiap.techchallenge.customer.domains.Customer;
import br.com.fiap.techchallenge.customer.dtos.CustomerDTO;
import br.com.fiap.techchallenge.customer.vos.Cpf;
import br.com.fiap.techchallenge.customer.vos.CustomerName;
import br.com.fiap.techchallenge.customer.vos.Email;

public class CustomerDTOToCustomer {
    private CustomerDTOToCustomer() {
        throw new IllegalStateException("Utility class");
    }

    public static Customer convert(CustomerDTO customerDTO) {
        return Customer.builder()
                .id(customerDTO.getId())
                .cpf(new Cpf(customerDTO.getCpf()))
                .name(new CustomerName(customerDTO.getName()))
                .email(new Email(customerDTO.getEmail()))
                .build();
    }
}
