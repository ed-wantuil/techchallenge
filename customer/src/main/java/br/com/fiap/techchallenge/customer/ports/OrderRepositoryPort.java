package br.com.fiap.techchallenge.customer.ports;

import java.util.List;

import br.com.fiap.techchallenge.customer.domains.Order;

public interface OrderRepositoryPort {
    Order checkout(Order order);

    List<Order> findByStatus(String status);
}
