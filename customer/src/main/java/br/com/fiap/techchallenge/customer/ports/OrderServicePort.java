package br.com.fiap.techchallenge.customer.ports;

import java.util.List;

import br.com.fiap.techchallenge.customer.dtos.OrderDTO;

public interface OrderServicePort {
    OrderDTO checkout(OrderDTO orderDTO);
    List<OrderDTO> findByStatus(String status);
}
