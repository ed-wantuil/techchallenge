package br.com.fiap.techchallenge.customer.ports;

import br.com.fiap.techchallenge.customer.dtos.CustomerDTO;

public interface CustomerServicePort {
    CustomerDTO create(CustomerDTO customerDTO);
    CustomerDTO findByCpf(String cpf);
}
