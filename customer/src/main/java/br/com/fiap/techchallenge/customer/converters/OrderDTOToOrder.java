package br.com.fiap.techchallenge.customer.converters;

import java.util.Objects;

import br.com.fiap.techchallenge.customer.domains.Order;
import br.com.fiap.techchallenge.customer.dtos.OrderDTO;
import br.com.fiap.techchallenge.customer.vos.OrderAmount;
import br.com.fiap.techchallenge.customer.vos.OrderItem;
import br.com.fiap.techchallenge.customer.vos.OrderStatus;

public class OrderDTOToOrder {
    private OrderDTOToOrder() {
        throw new IllegalStateException("Utility class");
    }

    public static Order convert(OrderDTO orderDTO) {
        return Order
                .builder()
                .id(orderDTO.getId())
                .customer(Objects.nonNull(orderDTO.getCustomer()) ? CustomerDTOToCustomer.convert(orderDTO.getCustomer()) : null)
                .items(orderDTO.getItems().stream()
                        .map(item -> OrderItem.builder()
                                .product(item.getProduct())
                                .quantity(item.getQuantity())
                                .price(item.getPrice())
                                .build())
                        .toList())
                .status(new OrderStatus(orderDTO.getStatus()))
                .created(orderDTO.getCreated())
                .amount(new OrderAmount(orderDTO.getAmount()))
                .build();
    }
}
