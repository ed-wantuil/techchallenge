package br.com.fiap.techchallenge.customer.enums;

public enum OrderStatusEnum {
    RECEIVED,
    PREPARING,
    READY,
    DONE
}
