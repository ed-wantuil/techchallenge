package br.com.fiap.techchallenge.customer.ports;

import br.com.fiap.techchallenge.customer.domains.Customer;

public interface CustomerRepositoryPort {
    Customer create(Customer customer);

    Customer findByCpf(String cpf);
}
