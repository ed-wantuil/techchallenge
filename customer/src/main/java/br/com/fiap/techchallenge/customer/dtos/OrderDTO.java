package br.com.fiap.techchallenge.customer.dtos;

import java.time.LocalDate;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@EqualsAndHashCode
@ToString
public class OrderDTO {
    private String id;
    private CustomerDTO customer;
    private List<OrderItemDTO> items;
    private String status;
    private LocalDate created;
    private Double amount;


    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @Getter
    @EqualsAndHashCode
    @ToString
    public static class OrderItemDTO {
        private String product;
        private Integer quantity;
        private Double price;
    }
}
