package br.com.fiap.techchallenge.customer.converters;

import java.util.Objects;

import br.com.fiap.techchallenge.customer.domains.Order;
import br.com.fiap.techchallenge.customer.dtos.OrderDTO;

public class OrderToOrderDTO {
    private OrderToOrderDTO() {
        throw new IllegalStateException("Utility class");
    }

    public static OrderDTO convert(Order order) {
        return OrderDTO
                .builder()
                .id(order.getId())
                .customer(Objects.nonNull(order.getCustomer()) ? CustomerToCustomerDTO.convert(order.getCustomer()) : null)
                .items(order.getItems().stream().map(item -> OrderDTO.OrderItemDTO.builder()
                        .product(item.getProduct())
                        .price(item.getPrice())
                        .quantity(item.getQuantity())
                        .build()).toList())
                .status(order.getStatus().getStatus().toString())
                .created(order.getCreated())
                .amount(order.getAmount().getAmount())
                .build();
    }
}
