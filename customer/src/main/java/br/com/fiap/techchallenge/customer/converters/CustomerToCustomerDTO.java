package br.com.fiap.techchallenge.customer.converters;

import br.com.fiap.techchallenge.customer.domains.Customer;
import br.com.fiap.techchallenge.customer.dtos.CustomerDTO;

public class CustomerToCustomerDTO {
    private CustomerToCustomerDTO() {
        throw new IllegalStateException("Utility class");
    }

    public static CustomerDTO convert(Customer customer) {
        return CustomerDTO.builder()
                .id(customer.getId())
                .cpf(customer.getCpf().getCpf())
                .name(customer.getName().getName())
                .email(customer.getEmail().getEmail())
                .build();
    }
}
