package br.com.fiap.techchallenge.customer.adapters;

import java.util.List;

import br.com.fiap.techchallenge.customer.converters.OrderDTOToOrder;
import br.com.fiap.techchallenge.customer.converters.OrderToOrderDTO;
import br.com.fiap.techchallenge.customer.dtos.OrderDTO;
import br.com.fiap.techchallenge.customer.ports.OrderRepositoryPort;
import br.com.fiap.techchallenge.customer.ports.OrderServicePort;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class OrderService implements OrderServicePort {
    private final OrderRepositoryPort orderRepositoryPort;

    @Override
    public OrderDTO checkout(OrderDTO orderDTO) {
        var order = OrderDTOToOrder.convert(orderDTO);
        order = orderRepositoryPort.checkout(order);

        return OrderToOrderDTO.convert(order);
    }

    @Override
    public List<OrderDTO> findByStatus(String status) {
        var orders = orderRepositoryPort.findByStatus(status);

        return orders
                .stream()
                .map(OrderToOrderDTO::convert)
                .toList();
    }
}
