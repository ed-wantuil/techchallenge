# Tech Challenge

This project was developed for postgraduate software architecture.

**DDD:**

```
https://miro.com/welcomeonboard/Z2hwMTJic3V1akhyZmxTNGtTMWNTWXp4VnVPa09rd1FIYTNac3RMQzBxa21IVXVXRFJZMG5EanRoZ2ZjV2NTNnwzMDc0NDU3MzYwMjEzNjU2NjU1fDI=?share_link_id=653145671350
```

**Run:**

```
docker-compose up
```

**Build:**

```
./build.sh
```

**Swagger:**
```
http://localhost:8080/swagger-ui/index.html
```

**Sample Postman:**
```
sample/postman.json
```

