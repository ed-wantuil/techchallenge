package br.com.fiap.techchallenge.attendant.ports;

import java.util.List;

import br.com.fiap.techchallenge.attendant.domains.Product;

public interface ProductRepositoryPort {
    List<Product> find();

    List<Product> findByCategory(String category);

    Product create(Product product);

    Product update(String id, Product product);

    void remove(String id);
}
