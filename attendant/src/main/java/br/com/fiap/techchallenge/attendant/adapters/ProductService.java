package br.com.fiap.techchallenge.attendant.adapters;

import java.util.List;

import br.com.fiap.techchallenge.attendant.converters.ProductDTOToProduct;
import br.com.fiap.techchallenge.attendant.converters.ProductToProductDTO;
import br.com.fiap.techchallenge.attendant.dtos.ProductDTO;
import br.com.fiap.techchallenge.attendant.ports.ProductRepositoryPort;
import br.com.fiap.techchallenge.attendant.ports.ProductServicePort;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ProductService implements ProductServicePort {
    private final ProductRepositoryPort productRepositoryPort;

    @Override
    public List<ProductDTO> find() {
        return productRepositoryPort
                .find()
                .stream()
                .map(ProductToProductDTO::convert)
                .toList();
    }

    @Override
    public List<ProductDTO> findByCategory(String category) {
        return productRepositoryPort
                .findByCategory(category)
                .stream()
                .map(ProductToProductDTO::convert)
                .toList();
    }

    @Override
    public ProductDTO create(ProductDTO productDTO) {
        var product = ProductDTOToProduct.convert(productDTO);
        product = productRepositoryPort.create(product);

        return ProductToProductDTO.convert(product);
    }

    @Override
    public ProductDTO update(String id, ProductDTO productDTO) {
        var product = ProductDTOToProduct.convert(productDTO);
        product = productRepositoryPort.update(id, product);

        return ProductToProductDTO.convert(product);
    }

    @Override
    public void remove(String id) {
        productRepositoryPort.remove(id);
    }
}
