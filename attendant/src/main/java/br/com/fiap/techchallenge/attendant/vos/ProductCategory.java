package br.com.fiap.techchallenge.attendant.vos;

import br.com.fiap.techchallenge.attendant.enums.ProductCategoryEnum;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class ProductCategory {
    ProductCategoryEnum category;

    public ProductCategory(String category) {
        validate(category);

        this.category = ProductCategoryEnum.valueOf(category);
    }

    private void validate(String category) {
        if (category == null) {
            throw new IllegalArgumentException("Category cannot be null");
        }
    }
}
