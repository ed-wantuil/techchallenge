package br.com.fiap.techchallenge.attendant.enums;

public enum ProductCategoryEnum {
    SNACK,
    DRINK,
    DESSERT,
    SIDE_DISH
}
