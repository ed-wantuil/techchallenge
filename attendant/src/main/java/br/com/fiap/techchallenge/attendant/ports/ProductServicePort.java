package br.com.fiap.techchallenge.attendant.ports;

import java.util.List;

import br.com.fiap.techchallenge.attendant.dtos.ProductDTO;

public interface ProductServicePort {
    List<ProductDTO> find();

    List<ProductDTO> findByCategory(String category);

    ProductDTO create(ProductDTO productDTO);

    ProductDTO update(String id, ProductDTO productDTO);

    void remove(String id);
}
