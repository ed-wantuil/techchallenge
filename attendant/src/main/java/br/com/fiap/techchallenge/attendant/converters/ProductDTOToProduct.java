package br.com.fiap.techchallenge.attendant.converters;

import br.com.fiap.techchallenge.attendant.domains.Product;
import br.com.fiap.techchallenge.attendant.dtos.ProductDTO;
import br.com.fiap.techchallenge.attendant.vos.ProductCategory;
import br.com.fiap.techchallenge.attendant.vos.ProductDescription;
import br.com.fiap.techchallenge.attendant.vos.ProductImage;
import br.com.fiap.techchallenge.attendant.vos.ProductName;
import br.com.fiap.techchallenge.attendant.vos.ProductPrice;

public class ProductDTOToProduct {
    private ProductDTOToProduct() {
        throw new IllegalStateException("Utility class");
    }

    public static Product convert(ProductDTO productDTO) {
        return Product.builder()
                .id(productDTO.getId())
                .name(new ProductName(productDTO.getName()))
                .category(new ProductCategory(productDTO.getCategory()))
                .price(new ProductPrice(productDTO.getPrice()))
                .description(new ProductDescription(productDTO.getDescription()))
                .image(new ProductImage(productDTO.getImage()))
                .build();

    }
}
