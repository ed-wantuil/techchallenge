package br.com.fiap.techchallenge.attendant.converters;

import br.com.fiap.techchallenge.attendant.domains.Product;
import br.com.fiap.techchallenge.attendant.dtos.ProductDTO;

public class ProductToProductDTO {
    private ProductToProductDTO() {
        throw new IllegalStateException("Utility class");
    }


    public static ProductDTO convert(Product product) {
        return ProductDTO.builder()
                .id(product.getId())
                .name(product.getName().getName())
                .category(product.getCategory().getCategory().name())
                .price(product.getPrice().getPrice())
                .description(product.getDescription().getDescription())
                .image(product.getImage().getImage())
                .build();
    }
}
