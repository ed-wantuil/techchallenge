package br.com.fiap.techchallenge.attendant.adapters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;

import java.util.List;

import br.com.fiap.techchallenge.attendant.domains.Product;
import br.com.fiap.techchallenge.attendant.dtos.ProductDTO;
import br.com.fiap.techchallenge.attendant.ports.ProductRepositoryPort;
import br.com.fiap.techchallenge.attendant.vos.ProductCategory;
import br.com.fiap.techchallenge.attendant.vos.ProductDescription;
import br.com.fiap.techchallenge.attendant.vos.ProductImage;
import br.com.fiap.techchallenge.attendant.vos.ProductName;
import br.com.fiap.techchallenge.attendant.vos.ProductPrice;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {
    @Mock
    ProductRepositoryPort productRepositoryPort;

    @InjectMocks
    ProductService productService;

    @Test
    void shouldFindAllProducts() {
        var products = List.of(Product
                .builder()
                .id("032002af-0432-4aba-af57-697d46b1ffbc")
                .name(new ProductName("Product Name"))
                .category(new ProductCategory("SNACK"))
                .price(new ProductPrice(10.0))
                .description(new ProductDescription("Product Description"))
                .image(new ProductImage("Product Image"))
                .build());

        var expected = List.of(ProductDTO
                .builder()
                .id("032002af-0432-4aba-af57-697d46b1ffbc")
                .name("Product Name")
                .category("SNACK")
                .price(10.0)
                .description("Product Description")
                .image("Product Image")
                .build());

        Mockito.when(productRepositoryPort.find()).thenReturn(products);

        var actual = productService.find();

        assertEquals(expected, actual);
    }

    @Test
    void shouldCreateProduct() {
        var product = Product
                .builder()
                .id("032002af-0432-4aba-af57-697d46b1ffbc")
                .name(new ProductName("Product Name"))
                .category(new ProductCategory("SNACK"))
                .price(new ProductPrice(10.0))
                .description(new ProductDescription("Product Description"))
                .image(new ProductImage("Product Image"))
                .build();

        var expected = ProductDTO
                .builder()
                .id("032002af-0432-4aba-af57-697d46b1ffbc")
                .name("Product Name")
                .category("SNACK")
                .price(10.0)
                .description("Product Description")
                .image("Product Image")
                .build();

        var request = ProductDTO
                .builder()
                .name("Product Name")
                .category("SNACK")
                .price(10.0)
                .description("Product Description")
                .image("Product Image")
                .build();

        Mockito.when(productRepositoryPort.create(any())).thenReturn(product);

        var actual = productService.create(request);

        assertEquals(expected, actual);
    }

    @Test
    void shouldUpdateProduct() {
        var product = Product
                .builder()
                .id("032002af-0432-4aba-af57-697d46b1ffbc")
                .name(new ProductName("Product Name"))
                .category(new ProductCategory("SNACK"))
                .price(new ProductPrice(10.0))
                .description(new ProductDescription("Product Description"))
                .image(new ProductImage("Product Image"))
                .build();

        var expected = ProductDTO
                .builder()
                .id("032002af-0432-4aba-af57-697d46b1ffbc")
                .name("Product Name")
                .category("SNACK")
                .price(10.0)
                .description("Product Description")
                .image("Product Image")
                .build();

        var request = ProductDTO
                .builder()
                .name("Product Name")
                .category("SNACK")
                .price(10.0)
                .description("Product Description")
                .image("Product Image")
                .build();

        Mockito.when(productRepositoryPort.update(any(), any())).thenReturn(product);

        var actual = productService.update("032002af-0432-4aba-af57-697d46b1ffbc", request);

        assertEquals(expected, actual);
    }
}
