package br.com.fiap.techchallenge.attendant.converters;

import static org.junit.jupiter.api.Assertions.assertEquals;

import br.com.fiap.techchallenge.attendant.domains.Product;
import br.com.fiap.techchallenge.attendant.dtos.ProductDTO;
import br.com.fiap.techchallenge.attendant.vos.ProductCategory;
import br.com.fiap.techchallenge.attendant.vos.ProductDescription;
import br.com.fiap.techchallenge.attendant.vos.ProductImage;
import br.com.fiap.techchallenge.attendant.vos.ProductName;
import br.com.fiap.techchallenge.attendant.vos.ProductPrice;
import org.junit.jupiter.api.Test;

class ProductDTOToProductTest {

    @Test
    void shouldConvertProductDTOToProduct() {
        var productDTO = ProductDTO
                .builder()
                .id("032002af-0432-4aba-af57-697d46b1ffbc")
                .name("Product Name")
                .category("SNACK")
                .price(10.0)
                .description("Product Description")
                .image("Product Image")
                .build();

        var expected = Product
                .builder()
                .id("032002af-0432-4aba-af57-697d46b1ffbc")
                .name(new ProductName("Product Name"))
                .category(new ProductCategory("SNACK"))
                .price(new ProductPrice(10.0))
                .description(new ProductDescription("Product Description"))
                .image(new ProductImage("Product Image"))
                .build();

        var actual = ProductDTOToProduct.convert(productDTO);

        assertEquals(expected, actual);
    }
}
